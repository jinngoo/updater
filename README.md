# Updater

#### 介绍
独立更新程序

#### 软件架构
.Net Core 3.1


#### 安装教程

1.  将Updater.exe放入要更新的程序根目录。
2.  创建Updater.json放入同级目录，`version`为当前版本，`server`为更新资源服务器地址。
3.  发布程序时，使用发布命令生成更新列表，并一起上传至服务器。

#### 使用说明

1.  `-p -v {version} [-d {description}]`或`-publish -version {version} [-description {description}]`: 根据exe所在文件夹的文件生成更新列表，之后随程序一起上传至服务器。
2.  `-c`或`-check`: 检查更新，输出之前生成的更新列表。
3.  `[-exe {exeName}]`: 执行更新，如果传入exe文件名(不包含扩展名)，则更新前关闭exe，更新后打开exe。

