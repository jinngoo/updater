﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.Json;

namespace Jinndev {

    class Program {



        static void Main(string[] args) {
            Directory.SetCurrentDirectory(new FileInfo(Process.GetCurrentProcess().MainModule.FileName).Directory.FullName);
            //Console.WriteLine("FileName: " + Process.GetCurrentProcess().MainModule.FileName);
            //Console.WriteLine("Environment: " + Environment.CurrentDirectory);
            //Console.WriteLine("Directory: " + Directory.GetCurrentDirectory());

            if (args.Length > 0) {
                if (Util.HasAnyArgs(args, "-publish", "-p")) {
                    PublishMode.Publish(args);
                    return;
                }
            }

            //PublishMode.Publish(args);
            UpdateMode.Update(args);

            //Console.WriteLine("Any key to close...");
            //Console.ReadKey();
        }

    }

}
