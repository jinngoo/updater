﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jinndev {

    class PublishInfo {

        public string version { get; set; }

        public List<FileEntry> entries { get; set; }

        public string description { get; set; }

        public long totalSize { get; set; }

        public long publishTime { get; set; }

    }

}
