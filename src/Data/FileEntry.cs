﻿namespace Jinndev {

    class FileEntry {

        public string path { get; set; }
        public long size { get; set; }
        public string md5 { get; set; }

    }

}
